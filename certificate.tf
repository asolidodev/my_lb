# The certificate
resource "aws_acm_certificate" "app_loadbalancer" {
  domain_name = "${var.domain_name}"

  subject_alternative_names = [
    "www.${var.domain_name}",
  ]

  validation_method = "DNS"

  tags = "${merge(
    map(
      "ManagedByTerraform", "true",
      "TerraformWorkspace", terraform.workspace,
    ),
    var.certificate_tags,
  )}"
}

# and validation
resource "aws_route53_record" "naked_cert_validation" {
  name    = "${aws_acm_certificate.app_loadbalancer.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.app_loadbalancer.domain_validation_options.0.resource_record_type}"
  zone_id = "${var.zone_id}"
  records = ["${aws_acm_certificate.app_loadbalancer.domain_validation_options.0.resource_record_value}"]
  ttl     = 60
}

resource "aws_route53_record" "www_cert_validation" {
  name    = "${aws_acm_certificate.app_loadbalancer.domain_validation_options.1.resource_record_name}"
  type    = "${aws_acm_certificate.app_loadbalancer.domain_validation_options.1.resource_record_type}"
  zone_id = "${var.zone_id}"
  records = ["${aws_acm_certificate.app_loadbalancer.domain_validation_options.1.resource_record_value}"]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "app_loadbalancer_cert_validation" {
  certificate_arn = "${aws_acm_certificate.app_loadbalancer.arn}"

  validation_record_fqdns = [
    "${aws_route53_record.naked_cert_validation.fqdn}",
    "${aws_route53_record.www_cert_validation.fqdn}",
  ]
}
