# Defines the Load Balancer for HTTP/HTTPS
resource "aws_lb" "app_loadbalancer" {
  name               = "${var.organization}-${var.environment}-lb-${var.lb_name}"
  load_balancer_type = "application"
  internal           = false

  security_groups = ["${aws_security_group.app_loadbalancer.id}"]
  subnets         = ["${var.aws_public_subnet_id}"]

  tags = "${merge(
    map(
      "ManagedByTerraform", "true",
      "TerraformWorkspace", terraform.workspace,
    ),
    var.my_lb_tags,
  )}"
}

# Defines the Target Group
resource "aws_lb_target_group" "target_group" {
  name     = "${var.lb_name}-80"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

# Defines the Listeners for HTTP
resource "aws_lb_listener" "listener_80" {
  load_balancer_arn = "${aws_lb.app_loadbalancer.id}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

# Defines the Listeners for HTTPS
resource "aws_lb_listener" "listener_443" {
  load_balancer_arn = "${aws_lb.app_loadbalancer.id}"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = "${aws_acm_certificate.app_loadbalancer.arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.target_group.id}"
    type             = "forward"
  }
}

# Attach Target Group to the instance
resource "aws_lb_target_group_attachment" "target_group_attachment" {
  target_group_arn = "${aws_lb_target_group.target_group.arn}"
  target_id        = "${var.instance_id}"
  port             = 80
}

# the DNS entries for the LB
resource "aws_route53_record" "naked" {
  zone_id = "${var.zone_id}"
  name    = "${var.domain}"
  type    = "A"

  alias {
    name                   = "${aws_lb.app_loadbalancer.dns_name}"
    zone_id                = "${aws_lb.app_loadbalancer.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "www" {
  zone_id = "${var.zone_id}"
  name    = "www"
  type    = "A"

  alias {
    name                   = "${aws_lb.app_loadbalancer.dns_name}"
    zone_id                = "${aws_lb.app_loadbalancer.zone_id}"
    evaluate_target_health = false
  }
}
