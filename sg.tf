resource "aws_security_group" "app_loadbalancer" {
  name        = "${var.organization}_${var.environment}_${var.domain}_lb_${var.lb_name}"
  description = "Security group for loadbalancer with port 443 open to the world"
  vpc_id      = "${var.vpc_id}"

  # HTTP/HTTPS Ingress
  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  # HTTP/HTTPS Egress
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.aws_public_subnet_cidr_blocks}"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.aws_public_subnet_cidr_blocks}"]
  }
}
