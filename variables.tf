variable "lb_name" {
  type = "string"
}

variable "aws_public_subnet_id" {
  type = "list"
}

variable "vpc_id" {
  type = "string"
}

variable "aws_public_subnet_cidr_blocks" {
  type = "list"
}

variable "instance_id" {
  type = "string"
}

variable "domain_name" {
  type = "string"
}

variable "zone_id" {
  type = "string"
}

variable "my_lb_tags" {
  type    = "map"
  default = {}
}

variable "certificate_tags" {
  type    = "map"
  default = {}
}

variable "environment" {}
variable "domain" {}
variable "organization" {}
